
Drupal.behaviors.geoblocker = function(context) {
  $('#geoblocker-ipv42long').click(function() {
    var ip = prompt(Drupal.t('Please enter a single IPv4 address in a dotted notation'), '');
    $.get('geoblocker/ipv42long/' + ip, function (data) {
      alert(data);
    });
    
    // prevent scroll position jump to the top due to the href="#" of the wrapped around <a>
    return false;
  });
};