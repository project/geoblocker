<?php 

/**
 * Implementation of hook_form
 */
function geoblocker_admin_settings() {
  $form = array();

  drupal_add_js(drupal_get_path('module', 'geoblocker') .'/geoblocker.js');
  
  $msg  = 'Determines how to process incoming requests. ';
  $msg .= '"Black listing" disallows access for visitors coming from configured countries. ';
  $msg .= '"White listing" allows access only for visitors coming from configured countires.';
  $form['geoblocker_mode'] = array(
    '#type' => 'select',
    '#title' => t('Access Control Method'),
    '#options' => array('white' => t('White listing'), 'black' => t('Black listing')),
    '#default_value' => variable_get('geoblocker_mode', 'black'),  
    '#description' => t($msg),
  );  
  
  $msg  = 'A comma separated list of 3-letter country codes in conformance to the ISO 3166 alpha 3 standard. ';
  $msg .= 'See !url for more information. !br';
  $msg .= 'Please make sure to disable any WYSIWYG editor like CKEditor or TinyMCE on this text box!';
  $form['geoblocker_countries'] = array(
    '#type' => 'textarea',
    '#title' => t('Countries'),
    '#default_value' => variable_get('geoblocker_countries', ''),
    '#description' => t($msg, array('!br' => '<br/>', '!url' => l('ISO 3166', 'http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3'))),
  );
  
  $msg  = 'The scope setting allows to configure the scope of the GeoBlocker module. ';
  $msg .= '"Complete Web Site" restricts/allows access to the complete Web Site. ';
  $msg .= '"A Subset of the Web Site" restricts/allows access only to a specific subset of nodes.';
  $form['geoblocker_scope'] = array(
    '#type' => 'select',
    '#title' => t('Access Control Scope'),
    '#options' => array('complete' => t('Complete Web Site'), 'subset' => t('A Subset of the Web Site')),
    '#default_value' => variable_get('geoblocker_scope', 'complete'),  
    '#description' => t($msg),
  );

  $msg  = 'A comma separated list of node IDs to apply the GeoBlocker module to. !br';
  $msg .= 'This setting is ignored if the selected scope is "Complete Web Site".';
  $form['geoblocker_scope_nodes'] = array(
    '#type' => 'textarea',
    '#title' => t('Nodes belonging to the Scope'),
    '#default_value' => variable_get('geoblocker_scope_nodes', ''),
    '#description' => t($msg, array('!br' => '<br/>')),  
  );
  
  $msg  = 'By default loopback requests, as well some other !reserved IPv4 ranges are allowed. ';
  $msg .= 'In this field you can put additional IPv4 ranges to be whitelisted and to have access to the site. !br';
  $msg .= 'Due performance considerations IPv4 addresses must be entered in unsigned !long representation. !br';
  $msg .= 'You could use !this JS tool to convert IPv4 dotted notation into IPv4 Decimal notation.';
  $whitelisted_ipv4_ranges = variable_get('geoblocker_whitelisted_ipv4s', array(array('2130706432', '2147483646')));
  $whitelisted_ipv4_form_string = '';
  $msg_whitelisted = '';
  foreach ($whitelisted_ipv4_ranges as $entry) {
    if ($entry[0] == $entry[1]) {
      $whitelisted_ipv4_form_string .= $entry[0] ."\r\n";
      $msg_whitelisted .= $entry[0] .' = '. long2ip($entry[0]);  
    }
    else {
      $whitelisted_ipv4_form_string .= $entry[0] .'|'. $entry[1] ."\r\n";
      $msg_whitelisted .= $entry[0] .'|'. $entry[1] .' = '. long2ip($entry[0]) .'|'. long2ip($entry[1]);
    }
  }
  $msg .= '!br'. $msg_whitelisted;
  $form['geoblocker_whitelisted_ipv4s'] = array(
    '#type' => 'textarea',
    '#title' => t('Whitelisted IPv4 ranges'),
    '#default_value' => $whitelisted_ipv4_form_string,
    '#description' => t($msg, 
      array(
        '!reserved' => l(t('reserved'), 'http://en.wikipedia.org/wiki/Reserved_IP_addresses#Reserved_IPv4_addresses'),      
        '!br' => '<br/>', 
        '!long' => l(t('long'), 'http://php.net/ip2long'),
        '!this' => '<a href="#"><span id="geoblocker-ipv42long">'. t('this') .'</span></a>',
      )
    ),  
  );
  
  $msg  = 'How often sould the system update the DB with country specific ranges. ';
  $msg .= 'Enter the time perdiod in seconds. ';
  $msg .= 'Time periods shorter than 30 are rather pointless since the growth rate of ';
  $msg .= 'IPv4 ranges is rather slow and changes of IPv4 ranges assigned to countries are very rare and uncommon.';
  $form['geoblocker_cron_ttl'] = array(
    '#type' => 'textfield',
    '#title' => t('DB Refresh Time'),
    '#default_value' => variable_get('geoblocker_cron_ttl', 60*60*24*30), // 30 days
    '#description' => t($msg)
  );
  
  $msg  = 'Default action if the system can not find the country the visitor originates from. ';
  $msg .= 'This setting is independant from the mode setting and is invoked only the system is unable ';
  $msg .= 'determine the country of origin of the visitor.'; 
  $form['geoblocker_default'] = array(
    '#type' => 'select',
    '#title' => t('Default action'),
    '#options' => array(0 => 'Block', 1 => 'Allow'),
    '#default_value' => variable_get('geoblocker_default', FALSE),
    '#description' => t($msg),
  );
  
  $form['#validate'] = array('geoblocker_admin_settings_validate');
  
  return system_settings_form($form);
}

/**
 * Implementation of hook_validate
 */
function geoblocker_admin_settings_validate($form, &$form_state) {
  $whitelisted_ipv4_ranges = $form_state['values']['geoblocker_whitelisted_ipv4s'];
  $whitelisted_ipv4_ranges = explode("\r\n", trim($whitelisted_ipv4_ranges));
  
  $new_whitelisted_ipv4_ranges = array();
  foreach ($whitelisted_ipv4_ranges as $entry) {
    $entry = trim($entry);
    if (strpos($entry, '|') === FALSE) {
      $entry = sprintf("%u", $entry);
      $new_whitelisted_ipv4_ranges[] = array($entry, $entry);    
    }
    else {
      $entry = explode('|', $entry);
      $new_whitelisted_ipv4_ranges[] = array(sprintf("%u", $entry[0]), sprintf("%u", $entry[1]));
    }
  }
  $form_state['values']['geoblocker_whitelisted_ipv4s'] = $new_whitelisted_ipv4_ranges;
}